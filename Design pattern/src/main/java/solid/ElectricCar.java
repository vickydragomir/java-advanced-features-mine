package solid;

public class ElectricCar implements Car {
     private Engine engine;

    public ElectricCar() {
        this.engine = new Engine;
    }

    void start(){
        System.out.print("Electric car")
        engine.run();

    }
    void accelerate(int kph){
        engine.speed(kph);

    }
    void stop (){
       engine.stop();
    }

  public void regeneratingBrake(){
        engine.stop();
      System.out.println("Regenerating battery");

  }

}



