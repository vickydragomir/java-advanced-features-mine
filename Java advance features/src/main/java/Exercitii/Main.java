package Exercitii;
public class Main {
    public static void main(String[] args) {
        Person man1 = new Student("Masterand", 1,5000,"Andrei","Bucuresti, Romania");
        Person man2= new Staff("Cristi", "Timisoara", "Medicina",60000);
        System.out.println(man1);
        System.out.println(man2);

        Person student = new Student("Student",2,4500);
        Person staff = new Staff("farmacie", 5300);
        System.out.println(student);
        System.out.println(staff);

        student.setName("Matei");
        student.setAddress("Cluj");
        staff.setName("Rares");
        staff.setAddress("Suceava");
        System.out.println(student);
        System.out.println(staff);

    }
    public static void printNameAddress(Person person){
        System.out.println(person.getName() + " - "+ person.getAddress());
    }

}
//Problema
      //  Person class
//Implement the Person abstract class. It should contain:
 //       two String fields: name , address
  //      non-arguments constructor which will set name , address elds as empty
  //      strings
    //    two-arguments constructor: String name , String address
    //    getter methods which will be responsible for returning name , address
    //    fields values
     //   setter methods which will be responsible for setting name , address elds
     //   values
     //   toString method which should return string in the following format: ?->? ,
     //   where ? is the name and adress value accordingly
      //  Student class
//Implement the Student class. It should extend the Person class.
  //      Implementation should meet the below criteria:
   //     three fields: type of study, year of study, study price
    //    three-arguments constructor: type of study, year of study, study price
     //   getter methods which will be responsible for returning declared fields
     //   setter methods which will be responsible for setting declared fields
     //   toString method which should return details information about a student
      //  Staff class
//Implement the Lecturer class. It should extend the Person class.
 //       Implementation should meet the below criteria:
  //      two fields: specialization, salary
 //       two-arguments constructor: specialization, salary
 //       getter methods which will be responsible for returning declared fields
  //      setter methods which will be responsible for setting declared fields
   //     toString method which should return detials information about a lecturer
   //     Please provide an example usage of above implementation.




